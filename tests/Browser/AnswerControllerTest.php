<?php

namespace Tests\Browser;

use App\User;
use App\Answer;
use App\Question;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\questionshow;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Browser\Pages\updateQuestionnAnswer;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AnswerControllerTest extends DuskTestCase
{
    use DatabaseMigrations;
    protected $questions;
    protected $user;
    protected $cleared=false;
    use withFaker;
    public function setUp():void{
        parent::setUp();

       if(!$this->cleared)
         {
             $this->artisan('migrate:fresh --seed');
             $this->questions = Question::with('user')->get()->random(1);
             $this->user =User::first();
            $this->cleared=true;
            $this->faker = \Faker\Factory::create();
       }
    }


    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_guest_can_not_view_create_answer_form()
    {
        $this->browse(function (Browser $browser) {
            $browser
            ->visit(new questionshow($this->questions[0]->slug))
            ->sleep(2)
            ->assertSee('Login To Submit Your Answer');
       });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_user_can_view_create_answer_form()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->user->email)
            ->visit(new questionshow($this->questions[0]->slug))
            ->sleep(2)
            ->assertDontSee('Login To Submit Your Answer');
       });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_answer_cant_be_empty()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->user->email)
            ->visit(new questionshow($this->questions[0]->slug))
            ->insertBankAnswer()
            ->assertSeeIn("@dusk-answer-body-error","The body field is required.");

       });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_user_can_submit_an_answer()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->user->email)
            ->visit(new questionshow($this->questions[0]->slug))
            ->insertAnswer('This is my answer to your question.')
            ->sleep(3)
            ->assertSee("This is my answer to your question.");

       });
    }


    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_user_can_see_update_button()
    {

        $this->browse(function (Browser $browser) {
          // print (env('APP_URL'));
          $answers =factory(Answer::class,2)->create([
            'body' =>  'body1 ',
            'user_id' =>$this->user->id,
            'question_id'=>$this->questions[0]->id,
          ]);
           $browser->loginAs($this->user->email)
           ->visit(new questionshow($this->questions[0]->slug))
           ->assertCanSeeUpdateLinks($answers,$this->questions[0])
           ;

       });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_user_can_update_their_answer()
    {

        $this->browse(function (Browser $browser) {
          // print (env('APP_URL'));
          $answers =factory(Answer::class,2)->create([
            'body' =>  'body1 ',
            'user_id' =>$this->user->id,
            'question_id'=>$this->questions[0]->id,
          ]);
          $updatedanswer='my new updated  answer';
           $browser->loginAs($this->user->email)
           ->visit(new updateQuestionnAnswer($this->questions[0]->id,$answers[0]->id))
           ->updateAnswser($updatedanswer);
           $this->assertDatabaseHas('answers', [
               'id'=>$answers[0]->id,
               'body'=>$updatedanswer
           ]);
           ;

       });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_user_cant_update_someone_elses_answer()
    {

        $this->browse(function (Browser $browser) {
          // print (env('APP_URL'));
          $answers =factory(Answer::class,2)->create([
            'body' =>  'body1 ',
            'user_id' =>$this->user->id,
            'question_id'=>$this->questions[0]->id,
          ]);
          $otheruser =User::where("id","!=",$this->user->id)->get()->first();
           $browser->loginAs($otheruser->email)
           ->visit(new updateQuestionnAnswer($this->questions[0]->id,$answers[0]->id))
           ->assertSee('This action is unauthorized')
           ;

       });
    }

/**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_user_can_delete_their_answer()
    {

        $this->browse(function (Browser $browser) {
          // print (env('APP_URL'));
          $answers =factory(Answer::class,2)->create([
            'body' =>  'body1 ',
            'user_id' =>$this->user->id,
            'question_id'=>$this->questions[0]->id,
          ]);
           $browser->loginAs($this->user->email)
           ->visit(new questionshow($this->questions[0]->slug))
           ->deleteAnswser($answers[0]->id);
           $this->assertDatabaseMissing('answers', [
               'id'=>$answers[0]->id,
               'body'=>$answers[0]->body,
               'question_id'=>$this->questions[0]->id,
           ]);


       });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_user_cant_delete_another_users_answer()
    {

        $this->browse(function (Browser $browser) {
          // print (env('APP_URL'));
          $answers =factory(Answer::class,2)->create([
            'body' =>  'body1 ',
            'user_id' =>$this->user->id,
            'question_id'=>$this->questions[0]->id,
          ]);
          $selector1= "@dusk-delete-answer-".$answers[0]->id;
          $selector2= "@dusk-delete-answer-".$answers[1]->id;
          $otheruser =User::where("id","!=",$this->user->id)->get()->first();
           $browser->loginAs($otheruser->email)
           ->visit(new questionshow($this->questions[0]->slug))
           ->assertMissing($selector1)
           ->assertMissing($selector2);

       });
    }



}
