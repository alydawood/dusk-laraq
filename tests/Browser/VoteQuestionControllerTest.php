<?php

namespace Tests\Browser;

namespace Tests\Browser;

use App\User;
use App\Answer;
use App\Question;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\questionshow;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Browser\Pages\updateQuestionnAnswer;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class VoteQuestionControllerTest extends DuskTestCase
{
    use DatabaseMigrations;
    protected $questions;
    protected $user;
    protected $cleared=false;
    use withFaker;
    public function setUp():void{
        parent::setUp();

       if(!$this->cleared)
         {
             $this->artisan('migrate:fresh --seed');
             $this->questions = Question::with('user')->get()->random(1);
             $this->user =User::first();
             $this->cleared=true;
             $this->faker = \Faker\Factory::create();
       }
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_guest_cant_vote_a_question()
    {
        $new_user=  factory(User::class,1)->create();
        $new_question = factory(Question::class,1)->create(['user_id'=>$new_user[0]->id]);
        $this->browse(function (Browser $browser) use($new_question) {
            $browser->visit(new questionshow($new_question[0]->slug))
            ->waitfor('@vote-question-count')
            ->click('@dusk-up-vote-question')
            ->assertPathIs('/login');

        });

    }

  /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_guest_cant_unvote_a_question()
    {
        $new_user=  factory(User::class,1)->create();
        $new_question = factory(Question::class,1)->create(['user_id'=>$new_user[0]->id]);
        $this->browse(function (Browser $browser) use($new_question) {
            $browser->visit(new questionshow($new_question[0]->slug))
            ->waitfor('@vote-question-count')
            ->click('@dusk-down-vote-question')
            ->assertPathIs('/login');

        });


    }


    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_user_can_vote_a_question()
    {
        $new_user=  factory(User::class,1)->create();
        $new_question = factory(Question::class,1)->create(['user_id'=>$new_user[0]->id]);


        $this->browse(function (Browser $browser) use($new_question) {
            $browser->loginAs($this->user->email)
            ->visit(new questionshow($new_question[0]->slug))
            ->click('@dusk-up-vote-question')
            ->assertSeeIn('@vote-question-count',1);
        });


    }

   /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_user_can_unvote_a_question()
    {
        $new_user=  factory(User::class,1)->create();
        $new_question = factory(Question::class,1)->create(['user_id'=>$new_user[0]->id]);
        $this->browse(function (Browser $browser) use($new_question) {
            $browser->loginAs($this->user->email)
            ->visit(new questionshow($new_question[0]->slug))
            ->click('@dusk-up-vote-question')
            ->assertSeeIn('@vote-question-count',1)
            ->waitfor('@vote-question-count')
            ->click('@dusk-down-vote-question')
            ->waitfor('@vote-question-count')
            ->assertSeeIn('@vote-question-count',-1);


        });


    }


}
