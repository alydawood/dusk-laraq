<?php

namespace Tests\Browser;

use App\User;
use App\Question;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\home;
use Tests\Browser\Pages\loginpage;
use Tests\Browser\Pages\questionEdit;
use Tests\Browser\Pages\questionshow;
use Tests\Browser\Pages\questioncreate;
use Tests\Browser\Pages\questionsindex;
use Illuminate\Foundation\Testing\DatabaseMigrations;
//HomePageTest
class QuestionControllerTest extends DuskTestCase
{

    use DatabaseMigrations;
    protected $questions;
    protected $user;
    protected $cleared=false;
    public function setUp():void{
        parent::setUp();

       if(!$this->cleared)
         {
             $this->artisan('migrate:fresh --seed');
             $this->questions = Question::with('user')->latest()->limit(5)->get();
             $this->user =User::first();
            $this->cleared=true;
       }
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_test_that_ask_question_button_is_present()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new home)
            ->sleep(1)
            ->assertSeeIn("@ask-question", 'Ask Question');
        });
    }
    /**
     *
     *
     * @return void
     *
     */
    public function it_tests_that_pagination_buttons_are_present()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new home)
            ->sleep(1)
            ->assertSeeIn("@page-older", 'Older')
            ->assertSeeIn("@page-newer", 'Newer');
        });
    }

    /**
     *
     *
     * @return void
     * @test
     */
    public function it_tests_if_questions_are_displayed()
    {
        $this->browse(function (Browser $browser) {

            $browser->visit(new home)
            ->sleep(10)
            ->assertSeenQuestions($this->questions)
            ;
        });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_test_if_logged_in_user_can_see_create_page()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user->email);
            $browser->visit(new home)
            ->sleep(1)
            ->assertSeeIn("@ask-question", 'Ask Question')
            ->click("@ask-question")
            ->assertPathIs('/questions/create')
            ->sleep(2)
            ->click('#navbarDropdown')
            ->click("div.dropdown-menu a.dropdown-item")
            ->assertGuest();
            ;
        });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_test_if_guest_can_not_see_create_page()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new home)
            ->sleep(1)
            ->assertSeeIn("@ask-question", 'Ask Question')
            ->click("@ask-question")
            ->assertPathIsNot('/questions/create')
            ;
        });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */

    public function it_tests_validation_for_creating_a_question(){
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user->email)
            ->visit(new questioncreate)
            ->assertValidateQuestion()
            ;
        });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */

    public function it_tests_a_user_can_create_a_question(){
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user->email)
            ->visit(new questioncreate)
            ->assertCreateQuestion()
            ;
        });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */

    public function it_tests_a_user_can_view_edit_page_for_the_question_they_submited(){
        $user=$this->user;
        $Questions=factory(Question::class,rand(10,15))->make(['body'=>'whyyyy']);
        $user->questions()
        ->saveMany(
            $Questions
        );
        $userquestions=Question::where('user_id','=',$user->id)->get()->random(10);
        $question_id= $userquestions[0]->id;
        $this->browse(function (Browser $browser) use($userquestions) {
            $browser->loginAs($this->user->email)
            ->sleep(1)
            ->visit(new questionEdit(strval($userquestions[0]->id)))
           ->assertCanViewEditQuestion($userquestions[0]);

        });

    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */

    public function it_tests_a_user_cant_view_edit_page_for_another_users_question(){
        $user=$this->user;
        $userquestions=Question::where('user_id','!=',$user->id)->get()->random(1);
        $question_id= $userquestions[0]->id;
        $this->browse(function (Browser $browser) use($question_id) {
            $browser->loginAs($this->user->email)
            ->sleep(1)
            ->visit(new questionEdit(strval($question_id)))
           ->assertCantViewEditQuestion()
           ;

        });

    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */

    public function it_tests_a_user_can_udpate_his_question(){
        $user=$this->user;
        $userquestions=Question::where('user_id','=',$user->id)->get()->random(1);
        $question_id= $userquestions[0]->id;
        $userquestions[0]->title = 'Updated ' . $userquestions[0]->title;
        $userquestions[0]->body = 'Body Updated ' . $userquestions[0]->body;
        $this->browse(function (Browser $browser) use($question_id,$userquestions) {
            $browser->loginAs($this->user->email)
            ->sleep(1)
            ->visit(new questionEdit(strval($question_id)))
            ->updateQuestion($userquestions[0])
            ->visit(new questionEdit(strval($question_id)))
            ->assertHasQuestionUpdated($userquestions[0]);

        });

    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */

    public function it_tests_a_user_can_delete_their_question(){
        $user=$this->user;
        $Questions=factory(Question::class,2)->make();
        $user->questions()->saveMany($Questions);
        $userquestions=Question::where('user_id','=',$user->id)->get()->last();
        $question_id= $userquestions->id;
        $this->browse(function (Browser $browser) use($question_id,$userquestions) {
            $browser->loginAs($this->user->email)
            ->sleep(1)
            ->visit(new questionsindex)
            ->deleteQuestion($question_id) ;
        });

    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */

    public function it_tests_can_view_question_and_answers(){

        $question=Question::where('answers_count','>',0)->get()->last();
        $this->browse(function (Browser $browser) use($question) {
            $browser->loginAs($this->user->email)
            ->sleep(1)
            ->visit(new questionshow($question->slug))
            ->assertHasQuestion($question)
            ->assertHasAnswers($question)
            ->sleep(3)
            ;

        });

    }

}
