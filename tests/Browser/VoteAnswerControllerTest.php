<?php

namespace Tests\Browser;

namespace Tests\Browser;

use App\User;
use App\Answer;
use App\Question;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\questionshow;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Browser\Pages\updateQuestionnAnswer;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class VoteAnswerControllerTest extends DuskTestCase
{
    use DatabaseMigrations;
    protected $questions;
    protected $user;
    protected $cleared=false;
    use withFaker;
    public function setUp():void{
        parent::setUp();

       if(!$this->cleared)
         {
             $this->artisan('migrate:fresh --seed');
             $this->questions = Question::with('user')->get()->random(1);
             $this->user =User::first();
             $this->cleared=true;
             $this->faker = \Faker\Factory::create();
       }
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_test_user_can_upvote_an_answer()
    {

        $answer = factory(Answer::class,1)
        ->create(
            [
                'body'=>'Answer vote test',
                'user_id'=>$this->user->id,
                'question_id'=>$this->questions[0]->id
           ]);
           $selector="@dusk-votes-answer-count-".$answer[0]->id;
          // $answer_count =   $answer.upVotes + $answer.downVotes;
        //   dd($answer[0]->votes_count);
        $this->browse(function (Browser $browser) use($answer,$selector) {
            $browser->loginAs($this->user->email)
            ->visit(new questionshow($this->questions[0]->slug))
            ->waitfor("@dusk-up-vote-answer-".$answer[0]->id)
            ->click("@dusk-up-vote-answer-".$answer[0]->id)
            ->waitfor($selector)
            ->assertSeeIn($selector, $answer[0]->votes_count +1);
            ;
        });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_test_user_can_downvote_an_answer()
    {

        $answer = factory(Answer::class,1)
        ->create(
            [
                'body'=>'Answer vote test',
                'user_id'=>$this->user->id,
                'question_id'=>$this->questions->id
           ]);

        $this->browse(function (Browser $browser) use($answer) {
            $browser->loginAs($this->user->email)
            ->visit(new questionshow($new_question[0]->slug))
            ->waitfor("@dusk-up-vote-answer-".$answer);
            ;
        });
    }
}
