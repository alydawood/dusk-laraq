<?php

namespace Tests\Browser;

use App\User;
use App\Answer;
use App\Question;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\questionshow;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Browser\Pages\updateQuestionnAnswer;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class FavoritesControllerTest extends DuskTestCase
{

    use DatabaseMigrations;
    protected $questions;
    protected $user;
    protected $cleared=false;
    use withFaker;
    public function setUp():void{
        parent::setUp();

       if(!$this->cleared)
         {
             $this->artisan('migrate:fresh --seed');
             $this->questions = Question::with('user')->get()->random(1);
             $this->user =User::first();
             $this->cleared=true;
             $this->faker = \Faker\Factory::create();
       }
    }
    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_user_can_favourite_a_question()
    {

        $new_user=  factory(User::class,1)->create();
        //dd($new_user);
        $new_question = factory(Question::class,1)->create(['user_id'=>$new_user[0]->id]);

        $this->browse(function (Browser $browser) use($new_question) {
            $browser->loginAs($this->user->email)
            ->visit(new questionshow($new_question[0]->slug))
            ->waitfor("@vote-question-count")
            ->sleep(1)
            ->favoriteaquestion($new_question[0],$this->user->id)
            ->visit(new questionshow($new_question[0]->slug))
            ->assertQuestionFavouriteCountEquals(1);

        });

    }
/**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_user_can_un_favourite_a_question()
    {

        $new_user=  factory(User::class,1)->create();
        $new_question = factory(Question::class,1)->create(['user_id'=>$new_user[0]->id]);
        $this->user->favorites()->attach($new_question[0]->id);
        $question=$new_question[0];

        $this->browse(function (Browser $browser) use($question) {
            $browser->loginAs($this->user->email)
            ->visit(new questionshow($question->slug))
            ->sleep(1)
            ->unFavouriteQuestion()
            ->assertQuestionFavouriteCountEquals(0);

        });

    }

  /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_tests_that_guest_can_not_favourite_a_question()
    {

        $this->browse(function (Browser $browser){
            $browser->visit(new questionshow($this->questions[0]->slug))
             ->sleep(1)
            ->click('@dusk-question-favorite')
            ->sleep(1)
            ->assertPathIs('/login')
            ;
        });

    }

}
