<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class loginpage extends Page
{
 protected $user;

   public function __construct($user){
     $this->user=$user;
   }
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/login';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    public function loginUser(Browser $browser){
        $browser->type('email',$this->user->email)
        ->type('password','password')
        ->sleep(2)
        ->press('Login')
        ->sleep(2)
        ;
    }
    public function loginInvalidUser(Browser $browser){
        print('hello from login');
        $browser->type('email',$this->user->email)
        ->type('password','passwordw')
        ->sleep(2)
        ->press('Login')
        ->sleep(2);
    }

    
}
