<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class updateQuestionnAnswer extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    protected  $qid,$ansid;
    public function __construct($qid,$ansid){
        $this->qid=$qid;
        $this->ansid=$ansid;
        ;
      }
    public function url()
    {

        return 'questions/'.$this->qid.'/answers/'.$this->ansid.'/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
     //  $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }
    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function updateAnswser(Browser $browser, $updatedAnswer){
        $browser
        ->value('@dusk-answer-update-body',$updatedAnswer)
         ->click('@dusk-answer-update');

    }


}
