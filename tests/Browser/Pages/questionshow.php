<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class questionshow extends Page
{
    protected $slug;
    public function __construct($slug){
        $this->slug=$slug;
      }


    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/questions/'.$this->slug;
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }
    public function  insertBankAnswer(Browser $browser){
        $browser
        ->sleep(2)
        ->value('@dusk-answer-body', '')
        ->click('@dusk-answer-submit')
        ;
    }
    public function  insertAnswer(Browser $browser,$answer){
        $browser
        ->waitFor('@dusk-answer-body')
        ->value('@dusk-answer-body', $answer)
        ->click('@dusk-answer-submit')
        ->waitFor('@dusk-success-message');
    }

    public function deleteAnswser(Browser $browser,$answerid){
        $selector= "@dusk-delete-answer-".$answerid;

        $browser->waitFor($selector)
        ->scrollTo($selector)
        ->click($selector)
        ->acceptDialog()
        ->sleep(1);
    }
    public function unFavouriteQuestion(Browser $browser){
        $selector="@dusk-question-favorite";
        $browser->waitFor($selector)
        ->scrollTo($selector)
        ->click($selector);
    }
    public function  assertQuestionFavouriteCountEquals(Browser $browser,$count){
         $browser->assertSeeIn('@dusk-question-favorite-count',$count);
     }
    public function assertFavouriteQuestionCount(Browser $browser,$count){
       $browser->assertSeeIn('@dusk-question-favorite-count',$count);
    }

    public function assertHasQuestion(Browser $browser, $question)
    {

        $body=substr($question->body,0,10);
        $browser
        ->waitFor('@dusk-question-title')
        ->assertSeeIn('@dusk-question-title',$question->title)
        ->assertSeeIn('@dusk-question-body',$body);
    }




    public function assertCanSeeUpdateLinks(Browser $browser, $useranswers,$question){

        // questions/#qid#/answers/#id#/edit


        foreach( $useranswers as $answer)
        {
            $answer_edit_link=env('APP_URL')."/questions/#qid#/answers/#id#/edit";
            $selector="@dusk-edit-answer-".$answer->id;

            $answer_edit_link=str_replace("#qid#",$question->id,$answer_edit_link);
            $answer_edit_link=str_replace("#id#",$answer->id,$answer_edit_link);
            $browser->sleep(2)
            ->assertElementHashref($selector,$answer_edit_link);
        }

    }

    public function assertCanotSeeUpdateLinks(Browser $browser, $useranswers,$question){

        // questions/#qid#/answers/#id#/edit


        foreach( $useranswers as $answer)
        {
            $answer_edit_link=env('APP_URL')."/questions/#qid#/answers/#id#/edit";
            $selector="@dusk-edit-answer-".$answer->id;

            $answer_edit_link=str_replace("#qid#",$question->id,$answer_edit_link);
            $answer_edit_link=str_replace("#id#",$answer->id,$answer_edit_link);
            $browser->sleep(2)
            ->assertElementDoesNotHavehref($selector,$answer_edit_link);
        }

    }

    public function assertHasAnswers(Browser $browser, $question)
    {
        $browser
        ->waitFor('@dusk-answers_count')
        ->assertSeeIn('@dusk-answers_count',$question->answers_count);
        foreach( $question->answers as $answer)
        {
            $body=substr($answer->body,0,15);
            $browser
            ->assertSeeIn('@dusk-answers-body-'.$answer->id,$body);


        }
    }

}
