<?php

namespace Tests\Browser\Pages;
use Laravel\Dusk\Browser;

class questioncreate extends Page
{
    use withFaker;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/questions/create';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    public function assertValidateQuestion(Browser $browser){
        $this->faker = \Faker\Factory::create();
        $browser
        ->sleep(1)
        ->value('@dusk-question-title', '')
        ->value('@dusk-question-body', '')
        ->click('@dusk-submit-question')
        ->sleep(5)
        ->assertSeeIn('@dusk-title-required','The title field is required.')
        ->assertSeeIn('@dusk-body-required','The body field is required.')
        ->assertElementHasClass('#question-title','is-invalid')//custom assert
        ->assertElementHasClass('#question-body','is-invalid') //custom assert
        ->value('@dusk-question-title',$this->faker->sentence(500,true))
        ->value('@dusk-question-body', 'dfdsf')
        ->click('@dusk-submit-question')
        ->sleep(5)
        ->assertSeeIn('@dusk-title-required','The title may not be greater than 255 characters.')
        ->assertElementHasClass('#question-title','is-invalid'); //custom assert

        ;
    }

    public function assertCreateQuestion(Browser $browser){
        $this->faker = \Faker\Factory::create();
        $browser
        ->sleep(1)
        ->value('@dusk-question-title',$this->faker->sentence(5,true))
        ->value('@dusk-question-body',$this->faker->sentence(100,true))
        ->click('@dusk-submit-question')
        ->sleep(2)
        ->assertPathIs('/questions')
        ->sleep(1)
        ->assertSeeIn('@dusk-success-message','Your question has been submitted')



        ;
    }
}
