<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class questionsindex extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/questions';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    public function deleteQuestion(Browser $browser, $question_id)
    {
        $browser->assertPathIs($this->url())
        ->waitFor('@dusk-delete-question-'.$question_id)
        ->scrollTo('@dusk-delete-question-'.$question_id)
        ->press('@dusk-delete-question-'.$question_id)
        ->acceptDialog()
        ->sleep(1)
        ->assertSee('Success! Your question has been deleted.');
    }
}
