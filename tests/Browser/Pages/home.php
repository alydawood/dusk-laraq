<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class home extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    public function assertSeenQuestions(Browser $browser,$questions){
        $index=0;
        foreach($questions as $question){
            $body=substr($question->excerpt,0,50);

            $browser
            ->assertSeeIn("@dusk-vote-".$question->id,$question->votes_count)
            ->assertSeeIn("@dusk-views-".$question->id,$question->views)
            ->assertSeeIn("@dusk-answers_count-".$question->id,$question->answers_count)
            ->assertSeeIn("@dusk-title-".$question->id,$question->title)
            ->assertSeeIn("@dusk-username-".$question->id,$question->user->name)
            ->assertSeeIn("@dusk-excerpt-".$question->id,$body);

             $index++;

        }

    }
}
