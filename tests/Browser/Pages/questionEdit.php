<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class questionEdit extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
     protected $qid;
     public function __construct($qid){
        $this->qid=$qid;
      }
     public function url()
    {
        return '/questions/'.$this->qid.'/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        //$browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    public function assertCanViewEditQuestion(Browser $browser,$userquestion){
       // print ($userquestion->title);
        $browser->assertPathIs($this->url())
        ->sleep(2)
        ->assertValue('@dusk-question-title',$userquestion->title) 
        ->assertValue('@dusk-question-body',$userquestion->body)        ;
    }
    public function assertHasQuestionUpdated(Browser $browser,$userquestion){
         $browser->assertPathIs($this->url())
         ->sleep(2)
         ->assertValue('@dusk-question-title',$userquestion->title) 
         ->assertValue('@dusk-question-body',$userquestion->body)        ;
     }
    public function updateQuestion(Browser $browser,$userquestion){
        // print ($userquestion->title);
         $browser->assertPathIs($this->url())
         ->sleep(2)
         ->value('@dusk-question-title',$userquestion->title) 
         ->value('@dusk-question-body',$userquestion->body)  
         ->click("@dusk-submit-question")
         ->sleep(2)
         ->assertSeeIn('@dusk-success-message','Success! Your question has been updated.')
         //dusk-success-message 
         ;
     }

    public function assertCantViewEditQuestion(Browser $browser){
        $browser->assertSee('This action is unauthorized');
    }
}
