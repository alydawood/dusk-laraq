<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginpage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AuthenticationTest extends DuskTestCase
{
    //use RefreshDatabase;
    use DatabaseMigrations;
    protected $user;
    protected $cleared=false;
    public function setUp():void{
        parent::setUp();

       if(!$this->cleared)
         {
             $this->artisan('migrate:fresh --seed');
             $this->user =User::first();
             $this->cleared=true;
       }
    }
    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_asserts_that_user_can_login()
    {
        $user = $this->user;
        $this->browse(function (Browser $browser)  use($user) {
            $browser->visit(new loginpage($user))
                    ->assertSee('Login')
                    ->loginUser()
                    ->assertPathIs('/home')
                    ->assertSee('Ask Question')
                    ->assertAuthenticated();

        });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_asserts_that_user_can_logout()
    {
        $user= $this->user;
        $this->browse(function (Browser $browser) use($user) {
            $browser->loginAs($user->email)
                   ->sleep(2)
                    ->visit('/home')
                    ->click('#navbarDropdown')
                    ->click("div.dropdown-menu a.dropdown-item")
                     ->assertGuest();
        });
    }
     /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function it_asserts_that_incorrect_login_fails()
    {
        $user= $this->user;
        $this->browse(function (Browser $browser)  use($user) {
            $browser->visit(new loginpage($user))
                    ->loginInvalidUser()
                    ->assertPathIs('/login')
                    ->sleep(1) //custom
                    ->assertSee('These credentials do not match our records.')
                    ->assertElementHasClass('input[type="email"]','is-invalid'); //custom assert


        });
    }
}
