<div class="row">
    <div class="col-md-6">
      <table>
          <tr>
          <td>
            <a title="This answer is useful"
                            class="vote-up {{ Auth::guest() ? 'off' : '' }}"
                            onclick="event.preventDefault(); document.getElementById('up-vote-answer-{{ $answer->id }}').submit();"
                            dusk="dusk-up-vote-answer-{{ $answer->id }}"
                            >
                            <i class="fas fa-plus-circle fa-2x {{ Auth::guest() ? 'off' : 'orange' }}"></i>
            </a>
            <form id="up-vote-answer-{{ $answer->id }}" action="/answers/{{ $answer->id }}/vote" method="POST" style="display:none;">
                @csrf
                <input type="hidden" name="vote" value="1">
            </form>
          </td>
          <td>
          <span class="votes-count"
          dusk="dusk-votes-answer-count-{{ $answer->id }}"
          >{{ $answer->votes_count }}</span>
          </td>
          <td>
          <a title="This answer is not useful"
                            class="vote-down {{ Auth::guest() ? 'off' : '' }}"
                            onclick="event.preventDefault(); document.getElementById('down-vote-answer-{{ $answer->id }}').submit();"
                            dusk="dusk-down-vote-answer-{{ $answer->id }}"
                            >
                            <i class="fas fa-minus-circle fa-2x {{ Auth::guest() ? 'off' : 'red' }}"></i>
                        </a>
                        <form id="down-vote-answer-{{ $answer->id }}" action="/answers/{{ $answer->id }}/vote" method="POST" style="display:none;">
                            @csrf
                            <input type="hidden" name="vote" value="-1">
         </td>              </form>
          </tr>
      </table>
    </div>

</div>
