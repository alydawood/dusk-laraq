@foreach ($answers as $answer)
@include('answers._voteSection',['answer'=>$answer])
<div class="media">


    <div class="media-body" dusk="dusk-answers-body-{{$answer->id}}">
        {!! $answer->body !!}
        <div>

            @can ('update', $answer)
        <a class="btn btn-sm btn-outline-info"
        href="{{ route('questions.answers.edit', [$question->id, $answer->id]) }}"
        dusk="dusk-edit-answer-{{$answer->id}}"
        >Edit</a>
            @endcan
            @can ('delete', $answer)
            <form class="form-delete" method="post" action="{{ route('questions.answers.destroy', [$question->id, $answer->id]) }}">
                @method('DELETE')
                @csrf
                <button class="btn btn-sm btn-outline-danger"
                dusk="dusk-delete-answer-{{$answer->id}}" onclick="return confirm('Are you sure?')"
                >Delete</button>
            </form>
            @endcan
        </div>
        <div class="float-right">
            <span class="text-muted">Answered {{ $answer->created_date }}</span>
            <div class="media mt-2">
                <a href="{{ $answer->user->url }}" class="pr-2">
                    <img src="{{ $answer->user->avatar }}">
                </a>
                <div class="media-body mt-1">
                    <a href="{{ $answer->user->url }}">{{ $answer->user->name }}</a>

                </div>

            </div>
        </div>
    </div>


</div>
<hr>


@endforeach
