@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        @include ('layouts._messages')
            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-items-center">

                        <h1 dusk="dusk-question-title">{{ $question->title }}</h1>
                        <div class="ml-auto">
                            <a href="{{ route('questions.index') }}" class="btn btn-outline-secondary">Back to all Questions</a>
                        </div>
                    </div>

                </div>

                <div class="card-body" dusk="dusk-question-body">

                    <div class="row">

                     <div class="col-md-3">
                        Votes
                        <table>
                        <tr>
                         <td>
                            <a title="This question is useful"
                             dusk="dusk-up-vote-question"
                            onclick="event.preventDefault(); document.getElementById('up-vote-question-{{ $question->id }}').submit();"
                            >
                            <i class="fas fa-plus-circle fa-2x {{ Auth::guest() ? 'off' : 'orange' }}"></i>
                    </a>

                    <form id="up-vote-question-{{ $question->id }}" action="/questions/{{ $question->id }}/vote" method="POST" style="display:none;">
                    @csrf
                    <input type="hidden" name="vote" value="1">
                    </form>
                        </td>
                         <td valign="middle">
                             <span  dusk="vote-question-count">{{ $question->votes_count }}</span>
                         </td>

                        <td>
                            <a title="This question is not useful"
                            dusk="dusk-down-vote-question"
                            onclick="event.preventDefault(); document.getElementById('down-vote-question-{{ $question->id }}').submit();"
                            >
                                <i class="fas fa-minus-circle fa-2x {{ Auth::guest() ? 'off' : 'red' }}"></i>
                            </a>

                            <form id="down-vote-question-{{ $question->id }}" action="/questions/{{ $question->id }}/vote" method="POST" style="display:none;">
                                @csrf
                                <input type="hidden" name="vote" value="-1">
                            </form>
                        </td>
                    </tr>
                          </table>
                    </div>
                        <div class="col-md-4 offset-md-3">
                        <a title="Click to mark as favorite question (Click again to undo)"
                    class="favorite mt-2 {{ Auth::guest() ? 'off' : ($question->is_favorited ? 'favorited' : '') }}"
                    onclick="event.preventDefault(); document.getElementById('favorite-question-{{ $question->id }}').submit();"
                     dusk="dusk-question-favorite"
                    >
                <i class="fas fa-star fa-2x {{$question->is_favorited ? 'star-orange' : ''}}" ></i>
                    <span class="favorites-count" dusk="dusk-question-favorite-count">{{ $question->favorites_count }}</span> favorite(s)
                    </a>
                    <form id="favorite-question-{{ $question->id }}" action="/questions/{{ $question->id }}/favorites" method="POST" style="display:none;">
                        @csrf
                        @if ($question->is_favorited)
                            @method ('DELETE')
                        @endif
                    </form>
                        </div>


                    </div>







                    <br>
                    {!! $question->body !!}
                   <div class="float-right">
                        <span class="text-muted">Answered {{ $question->created_date }}</span>
                        <div class="media mt-2">
                            <a href="{{ $question->user->url }}" class="pr-2">
                                <img src="{{ $question->user->avatar }}">
                            </a>
                            <div class="media-body mt-1">
                                <a href="{{ $question->user->url }}">{{ $question->user->name }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h2 dusk="dusk-answers_count">{{ $question->answers_count . " " . str_plural('Answer', $question->answers_count) }}</h2>
                    </div>
                    <hr>
                    @include('answers._questionAnswers',['answers'=>$question->answers])

                </div>
            </div>
        </div>


    </div>

    @auth
        @include ('answers._create')
    @endauth
    @guest
    <div class="col-md-12">
        <p class="text-center">Login To Submit Your Answer.</p>
    </div>
    @endguest

</div>


</div>

@endsection
