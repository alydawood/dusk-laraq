<?php

namespace App\Providers;

use Laravel\Dusk\Browser;
use Illuminate\Support\ServiceProvider;
use PHPUnit\Framework\Assert as PHPUnit;

class DuskServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Browser::macro('assertElementHasClass', function ($element = null,$classname = null) {
           //input[type="email"]
            $classes = $this->attribute($element,'class')  ;
                    $classes = explode(" ",$classes);
                    PHPUnit::assertContains($classname,$classes,
                    "Did not see value '$classname' in element $element.");

            return $this;
        });
        Browser::macro('assertElementHashref', function ($element = null,$link = null) {
            //input[type="email"]
             $hreflink = $this->attribute($element,'href')  ;
                     PHPUnit::assertEquals($hreflink,$link,
                     "Did not see value '$link' in element $element.");

             return $this;
         });
         Browser::macro('assertElementDoesNotHavehref', function ($element = null,$link = null) {
            //input[type="email"]
             $hreflink = $this->attribute($element,'href')  ;
                     PHPUnit::assertNotEquals($hreflink,$link,
                     "Did not see value '$link' in element $element.");

             return $this;
         });
         Browser::macro('favoriteaquestion', function ($question,$userid) {
            if( $question->favorites()->where('user_id', $userid)->count() >0 ){
                $question->favorites()->detach($userid);
            }
            $question->favorites()->attach($userid);
            return $this;
        });
        Browser::macro('sleep', function ($seconds = 1) {
             sleep($seconds);
             return $this;
         });
    }
}
