<?php

use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(2),
        'body' => $faker->paragraphs(rand(3, 7), true),
        'views' => rand(0, 10),
    ];
});
